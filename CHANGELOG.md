# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.8](https://gitlab.com/newtee-public/nuxt-sanitize-html/compare/v0.1.7...v0.1.8) (2022-04-08)

### [0.1.7](https://gitlab.com/newtee-public/nuxt-sanitize-html/compare/v0.1.6...v0.1.7) (2022-04-08)

### [0.1.6](https://gitlab.com/newtee-public/nuxt-sanitize-html/compare/v0.1.5...v0.1.6) (2022-04-07)

### [0.1.5](https://gitlab.com/newtee-public/nuxt-sanitize-html/compare/v0.1.4...v0.1.5) (2022-04-07)

### [0.1.4](https://gitlab.com/newtee-public/nuxt-sanitize-html/compare/v0.1.3...v0.1.4) (2022-04-07)

### 0.1.3 (2022-04-07)

### [0.0.5](https://github.com/HarmNullix/nuxt-sanitize-html/compare/v0.0.4...v0.0.5) (2022-04-07)

### [0.0.4](https://github.com/HarmNullix/nuxt-sanitize-html/compare/v0.0.3...v0.0.4) (2022-04-07)

### [0.0.3](https://github.com/HarmNullix/nuxt-sanitize-html/compare/v0.0.2...v0.0.3) (2022-04-07)

### 0.0.2 (2022-04-07)
