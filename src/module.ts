import { dirname, resolve } from 'pathe';
import {fileURLToPath} from 'url'
import {defineNuxtModule, addPluginTemplate} from '@nuxt/kit'
import {IOptions} from "sanitize-html";

export interface ModuleOptions extends IOptions {
}

export default defineNuxtModule<ModuleOptions>({
  meta: {
    name: 'sanitize-html',
    configKey: 'sanitizeHtml',
    compatibility: {
      nuxt: '^3.0.0'
    }
  },
  defaults: {
  },
  async setup(options, nuxt) {
    // @ts-ignore
    const __filename = fileURLToPath(import.meta.url);
    const __dirname = dirname(__filename);

    addPluginTemplate({
      src: resolve(__dirname, './runtime/plugin.mjs'),
      filename: 'sanitizeHtml.options.mjs',
      options: {asString: JSON.stringify(options || {}, )}
    });
  }
})
