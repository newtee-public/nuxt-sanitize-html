// @ts-ignore
import {defineNuxtPlugin} from '#app'
// @ts-ignore
import sanitizeHtml, {defaults, IOptions} from "sanitize-html";

export interface sanitize {
  (dirty: string, options?: IOptions): string
}

// @ts-ignore
declare module 'vue/types/vue' {
  interface Vue {
    $sanitize: sanitize
  }
}

// Nuxt 3
// @ts-ignore
interface PluginInjection {
  $sanitize: sanitize
}

// Nuxt Bridge & Nuxt 3
// @ts-ignore
declare module '#app' {
  interface NuxtApp extends PluginInjection {
  }
}


// @ts-ignore
declare module '@vue/runtime-core' {
  interface ComponentCustomProperties extends PluginInjection { }
}

export default defineNuxtPlugin((nuxtApp) => {
  const customOptions = JSON.parse('"<%= options.asString %>"'.replace(/^"(.*)"$/gmi, '$1'));
  const sanitize = (dirty, opts = null) => sanitizeHtml(dirty, opts);

  const install = (app, options) =>
    app.config.globalProperties.$sanitize = (dirty, opts = {}) => {
      return sanitize(dirty, {
        ...opts, ...options
      })
    }

  nuxtApp.vueApp.use({install}, {
    ...defaults,
    ...customOptions,
  })

  return {
    provide: {
      sanitize: nuxtApp.vueApp.config.globalProperties.$sanitize
    },
  }
})
