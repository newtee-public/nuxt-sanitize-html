import {IOptions} from "sanitize-html";

export interface sanitize {
  (dirty: string, options?: IOptions): string
}

// @ts-ignore
declare module 'vue/types/vue' {
  interface Vue {
    $sanitize: sanitize
  }
}

// Nuxt 3
// @ts-ignore
interface PluginInjection {
  $sanitize: sanitize
}

// Nuxt Bridge & Nuxt 3
// @ts-ignore
declare module '#app' {
  interface NuxtApp extends PluginInjection {
  }
}


// @ts-ignore
declare module '@vue/runtime-core' {
  interface ComponentCustomProperties extends PluginInjection { }
}
