import { defineNuxtConfig } from 'nuxt3'
import SanitizeHtml from '..'

export default defineNuxtConfig({
  ssr: false,
  target: "static",
  modules: [
    SanitizeHtml
  ],
  sanitizeHtml: {
    addPlugin: true,
    override: {
      allowedAttributes: {
        h3: ['class']
      }
    }
  }
})
