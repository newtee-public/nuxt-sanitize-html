# Sanitize HTML for Nuxt
For usage of sanitize-html, see [the official repo](https://github.com/apostrophecms/sanitize-html)
Usage:
```ts
// nuxt.config
{
  buildModules: [
    'nuxt-sanitize-html',
  ],
  sanitizeHtml: {
    allowedAttributes: {
      h3: ['style']
    }
  }
}
```

```vue
<template>
  <div>
<!--    sanitize in template-->
    <div v-html="$sanitize(dirty)"></div>
  
<!--    or before-->
    <div v-html="clean"></div>
  </div>
</template>

<script setup lang="ts">
import {useNuxtApp} from "nuxt3/app";

// <!--    sanitize in template-->
const dirty = '<h3 style="color:red" onmouseenter="alert(`Something else`)">I do not alert something!</h3>'

// <!--    or before-->
const nuxtApp = useNuxtApp()
const clean = nuxtApp.$sanitize(dirty)
</script>
```


## Development

- Run `npm run dev:prepare` to generate type stubs.
- Use `npm run dev` to start [playground](./playground) in development mode.

I'm as new as anyone to Nuxt modules, so PR's to make this module better are appreciated!
